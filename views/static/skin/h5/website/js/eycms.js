function setTab(name,cursel,n){
    try {
		for(i=1;i<=n;i++){
			var menu=$('#'+name+i);
			var con=$("#con_"+name+"_"+i);
			menu[0].className=i==cursel?"active-on":"";
			con[0].style.display=i==cursel?"block":"none";
		}
	}
    catch (e) {}
}

/* 搜索 */
function checksearch(the) {
	if (the.key.value == "") {
		alert("请输入模板名称或编号");
		the.key.focus();
		return false
	}
	if (the.key.value == "请输入模板名称或编号") {
		alert("请输入模板名称或编号");
		the.key.focus();
		return false
	}
}

/* 收藏 */
function addfav(id) {
	var url, data;
	url = webroot + "user/ajax.asp";
	data = "act=favorite";
	data += "&t0=" + encodeURIComponent(id);
	$.ajax({
		type: "post",
		cache: false,
		url: url,
		data: data,
		success: function(_) {
			var act = _.substring(0, 1);
			var info = _.substring(1);
			switch (act) {
			case "0":
				$.fn.tips({
					type: "error",
					content: info
				});
				break;
			case "1":
				$.fn.tips({
					type: "error",
					content: info
				});
				break;
			case "2":
				$.fn.tips({
					type: "ok",
					content: info
				});
				break;
			default:
				alert(_);
				break;
			}
		}
	});
	return false
}
/* 滚动加载 */
var startHref;
$(function() {
	function item_masonry() {
		$('.case-item img').load(function() {
			$('.case-item-loop').masonry({
				itemSelector: '.cloop'
			});
		});
		$('.case-item-loop').masonry({
			itemSelector: '.cloop'
		});
	}
	var p = false;
	if ($(".case-item").length > 0) {
		p = true;
		item_masonry();
	}
	$(".load-more a").click(function() {
		var href = $(this).attr("href");
		startHref = href;
		if (href != undefined) {
			$.ajax({
				type: "post",
				cache: false,
				url: startHref,
				success: function(data) {
					var $result = $(data).find(".cloop");
					if (p) {
						$(".case-item-loop").append($result).masonry('appended', $result);
						item_masonry();
					} else {
						$(".case-item-loop").append($result);
					}
					var newHref = $(data).find(".load-more a").attr("href");
					if (newHref != "") {
						$(".load-more a").attr("href", newHref);
					} else {
						$(".load-more").html('');
					}
				}
			})
		}
		return false;
	});
	$(window).bind("scroll", function() {
		if ($(document).scrollTop() + $(window).height() > $(document).height() - 226) {
			if ($(".load-more a").attr('href') != startHref) {
				$(".load-more a").trigger("click");
			}
		}
	})
});
/* 更多加载 */
var startHref;
$(function() {
	var leftNavMore = $(".left-navMore");
	var leftNav = $(".left-nav");
	leftNavMore.hover(function() {
		leftNav.show()
	}, function() {
		leftNav.hide()
	});
	
	$(".wap-ico").mouseover(function() {
		$(this).parent().parent().find(".mb-ico").show(200);
		$(this).parent().find(".wap-ico").hide(200);
	});
	$(".mb-ico").mouseleave(function() {
		$(this).parent().find(".mb-ico").hide(200);
		$(this).parent().find(".wap-ico").show(200);
	});

	$(".page-more a").click(function() {
		var href = $(this).attr("href");
		startHref = href;
		if (href != undefined) {
			$.ajax({
				type: "get",
				cache: false,
				url: startHref,
				success: function(data) {
					var $result = $(data).find(".loop");
					$(".item-loop").append($result);
					var newHref = $(data).find(".page-more a").attr("href");
					if (newHref != "") {
						$(".page-more a").attr("href", newHref)
					} else {
						$(".page-more").html('<div class="xz-tac pl-more page-more">没有了</div>')
					}
				}
			})
		}
		return false
	})
});

if(navigator.userAgent.indexOf("MSIE")>0)
{
	if(navigator.userAgent.indexOf("MSIE 6.0")>0){var ie6 = true;}   
	if(navigator.userAgent.indexOf("MSIE 7.0")>0){var ie7 = true;}   
	if(navigator.userAgent.indexOf("MSIE 8.0")>0){var ie8 = true;}   
	if(navigator.userAgent.indexOf("MSIE 9.0")>0){var ie9 = true;} 
}
var ease='';
ie6?ease="":ease='easeOutQuart';

function token(e){	
	var bw=window.screen.width;
	var st=$("body").scrollTop();
	var purl=$(e).attr("data-fr");
	
	if(ie6||ie7||ie8){
		return false;
	}
	else{
		$(e).attr("href",'javascript:void(0)')
	}

	$("body").css({"overflow":"hidden"});
	$("#fra").attr("src",purl);
	$(".header").hide();
	$(".help-nav").hide();
	$(".class-nav").hide();
	$(".nav").css({"position":"fixed"});
	$(".nav").animate({"top":"0"},100,ease);
	$("#frame").stop(false).show(30).animate({"left":0,"width":bw},200,ease);
	$(".Cmain").show();
	$(".Cmain").animate({"position":"relative","left":"-"+bw+"px"},300,ease);
	window.location.hash="#zoom";
}

function goF()
{  
	window.parent.location.hash="#home";
	var bw=window.screen.width;
	$("body").css({"overflow":"visible"});
	$(".nav").animate({"top":"140px"},300,ease);
	$(".nav").css({"position":"inherit"});
	$(".header").show();
	$("#frame").stop(false).hide(30).animate({"left":bw+"px"},200,ease).css("width",bw);
	$(".help-nav").show();
	$(".class-nav").show();
	$(".Cmain").animate({"position":"inherit","left":0},200,ease);
}