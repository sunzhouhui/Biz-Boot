// 选项卡
function tabs(a, b) {
    var $maxWapDom = a;
    var $list = $maxWapDom.children('.tabs-nav').find('li');
    var $items = $maxWapDom.find('.tabs-item');
    var sel = b;

    $items.first().addClass(sel);

    $list.hover(function() {
        var nIdx = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $items.eq(nIdx).addClass(sel).siblings().removeClass(sel);
    })
}

tabs($('.m-feature'), 'tabs-item-on');
tabs($('.m-case'), 'tabs-item-on');

//实例化动画插件
var wow = new WOW({
    boxClass: 'wow',
    animateClass: 'animated',
    offset: 0,
    mobile: true,
    live: true
});
wow.init();


//轮播图
var mBanner = $('.m-slide').unslider({
    fluid: true,
    dots: true
});
data04 = mBanner.data('unslider');
$('.u-control').click(function () {
    var fn = this.className.split(' ')[1];
    data04[fn]();
})

//新闻轮播
newsListFn();

function newsListFn() {
    var speed = 4000;
    var oUl = $('.info-list');
    var oList = $('.info-list li');
    var oLen = oList.length;
    var oWidth = oList.eq(0).innerWidth();
    var oFocus = $('.slide .u-pointer');
    var num = 0;
    var timer = null;

    oUl.append(oList.eq(0).clone()).css('width', (oLen + 1) * oWidth);

    for (var i = 0; i < oLen; i++) {
        if (i == 0) {
            oFocus.append('<li class="focus"></li>')
        } else {
            oFocus.append('<li></li>')
        }
    }

    $('li', oFocus).mouseover(function() {
        $(this).addClass('focus').siblings().removeClass('focus');

        var idx = $(this).index();
        oUl.stop().animate({
            left: -idx * oWidth + 'px'
        }, 500);
        num = idx;
    })

    function autoPlay() {

        num++;

        if (num > oLen) {
            oUl.css({
                left: 0
            }).stop().animate({
                left: -oWidth
            }, 500);
            num = 1;
        } else {
            oUl.stop().animate({
                left: -num * oWidth
            }, 500);
        }

        if (num == oLen) {
            $('li', oFocus).eq(0).addClass('focus').siblings().removeClass('focus');
        } else {
            $('li', oFocus).eq(num).addClass('focus').siblings().removeClass('focus');
        }

    }

    timer = setInterval(autoPlay, speed);

    $('.slide').mouseenter(function() {
        clearInterval(timer);
    }).mouseleave(function() {
        timer = setInterval(autoPlay, speed);
    })
}

//返回顶部
(function() {
    $.backTop = function(element, speed, invisible, opacity) {
        var $oEle = element;
        (speed === undefined) ? speed = 500: speed = speed;
        (invisible === undefined) ? invisible = false: invisible = invisible;
        (opacity === undefined) ? opacity = '' : opacity = opacity;
        if (invisible) {
            var oWin = $(window);
            var oDoc = $(document);

            $oEle.hide();
            oWin.scroll(function() {
                if (oDoc.scrollTop() > 1) {
                    $oEle.fadeIn(opacity);
                } else {
                    $oEle.fadeOut(opacity);
                }
            })
        }
        $oEle.click(function() {
            $('html, body').stop().animate({ scrollTop: 0 }, speed);
            return false;
        })
    }
})(jQuery)

$.backTop($('#j-backTop'), 500, false);

$('.side-nav a').each(function(){
    //移除所有类名
    $(this).removeClass('active');
    //当前添加
    $(this).hover(function(){
        $(this).addClass('active').siblings().removeClass('active');
    })
})