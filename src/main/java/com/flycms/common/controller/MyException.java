package com.flycms.common.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 报错实现错误代码的处理
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 1:33 2019/8/29
 */

@Controller
public class MyException implements ErrorController {

    /**
     * 该方法和以上方法不能同时共存，因为@RequestMapping("/error")相同
     * 异常的统一处理
     *是有的错误都到这个页面
     * @return
     */
    @Override
    @RequestMapping("/error")
    public String getErrorPath() {
        //获取statusCode:401,404,500
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        switch (statusCode) {
            case 400:
                return "/400";
            case 403:
                return "/403";
            case 404:
                return "/404";
            default:
                return "/500";
        }
    }

    //无权限
    @GetMapping("/403{url.suffix}")
    public String noPermission ()
    {
        return "403";
    }

    //页面不存在
    @GetMapping("/404{url.suffix}")
    public String noPage()
    {
        return "404";
    }

    //页面错误
    @GetMapping("/500{url.suffix}")
    public String errorPage()
    {
        return "500";
    }
}

