package com.flycms.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.springframework.util.StringUtils;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 通用拦截器
 * @email 79678111@qq.com
 * @Date: 14:35 2019/11/21
 */
public class JsonUtil {

  // 对象转json
  public static String objectToJson(Object object) {
    if (object == null) return null;
    return JSON.toJSONString(object);
  }

  // json转对象
  public static <T> T jsonToObject(String json, Class<T> object) {
    if (StringUtils.isEmpty(json)) return null;
    return JSON.parseObject(json, object);
  }

  // 带泛型的json转对象
  public static <T> T jsonToObject(String json, TypeReference<T> type) {
    if (StringUtils.isEmpty(json)) return null;
    return JSON.parseObject(json, type.getType());
  }
}
