package com.flycms.annotation;

import com.flycms.common.utils.result.Result;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Method;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 22:29 2019/11/5
 */
@Aspect
@Component
public class DoLogAspect {

    @Around(value = "@annotation( com.flycms.annotation.AspLog )")
    public Object aroundExec(ProceedingJoinPoint pjp) throws Throwable {

        MethodSignature ms = (MethodSignature) pjp.getSignature();
        Method method = ms.getMethod();

        AspLog myAnnotation = method.getAnnotation(AspLog.class);
        System.out.println(myAnnotation.value());
        return pjp.proceed();
    }
}
