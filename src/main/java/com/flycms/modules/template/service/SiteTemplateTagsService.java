package com.flycms.modules.template.service;

import com.flycms.common.utils.result.Result;
import com.flycms.modules.template.entity.SiteTemplateTags;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 14:43 2019/11/21
 */
public interface SiteTemplateTagsService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 添加用户自定义模板标签
     *
     * @param siteTemplateTags
     * @return
     */
    @Transactional
    public Result addSiteTemplateTags(SiteTemplateTags siteTemplateTags);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按网站当前网站id和当前使用模板id查询相关所有自定义标签
     *
     * @param siteId
     * @param templateId
     * @return
     */
    public List<SiteTemplateTags> selectSiteTemplateTagsList(Long siteId, Long templateId);
}
