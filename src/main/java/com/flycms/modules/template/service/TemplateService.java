package com.flycms.modules.template.service;

import com.flycms.common.pager.Pager;
import com.flycms.modules.template.entity.Template;
import com.flycms.modules.template.entity.TemplatePage;
import com.flycms.modules.template.entity.TemplateSku;
import com.flycms.modules.template.entity.TemplateVO;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 16:09 2019/8/15
 */
public interface TemplateService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    public Object addDeveloperTemplate(Template template);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 修改模板主表信息
     *
     * @param template
     * @return
     */
    public Object updateDeveloperTemplatePage(Template template);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 错误提示页面
     *
     * @param model
     * @param message
     * @return
     */
    public String getException(Model model,String message);
    /**
     * 按id查询模板信息
     *
     * @param id
     * @return
     */
    public Template findById(Long id);

    /**
     * 按模板id查询SKU信息
     *
     * @param templateId
     * @return
     */
    public List<TemplateSku> selectTemplateSku(Long templateId);

    public String getAdminTemplate(String template);

    /**
     * 查询用户所有发布的模板列表
     *
     * @return
     */
    public List<TemplateVO> selectTemplateList();

    /**
     * 后台模板列表翻页
     *
     * @param template
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Pager<Template> selectTemplatePager(Template template, Integer page, Integer pageSize, String sort, String order);

}
