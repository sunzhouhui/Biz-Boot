package com.flycms.modules.template.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 11:12 2019/9/16
 */
@Setter
@Getter
public class TemplateSku implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;			        //编号
    private Long templateId;
    private Integer useTime;   //添加时间
    private BigDecimal price;       //价格
}
