package com.flycms.modules.template.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.template.entity.TemplateSku;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface TemplateSkuDao extends BaseDao<TemplateSku> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

}
