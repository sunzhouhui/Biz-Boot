package com.flycms.modules.finance.controller;

import com.flycms.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 网站销售信息
 * @email 79678111@qq.com
 * @Date: 12:57 2019/12/11
 */
@Controller
@RequestMapping("/member/buy/")
public class SiteBuyMemberController extends BaseController {

    @GetMapping("/buy_site{url.suffix}")
    public String buyStieInfo(Model model) {
        return "system/member/order/buy_info";
    }

}
