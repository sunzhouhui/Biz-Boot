package com.flycms.modules.finance.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.finance.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface OrderDao extends BaseDao<Order> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询该企业指定产品类型的产品订单id列表
     *
     * @param companyId
     *         企业id
     * @param marketType
     *        产品类型
     * @return
     */
    public Set<Long> queryOrderByProductIdList(@Param("companyId") Long companyId, @Param("marketType") Integer marketType);
}
