package com.flycms.modules.site.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.utils.StringUtils;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.site.service.SiteColumnService;
import com.flycms.modules.site.service.SiteService;
import com.flycms.modules.site.service.SiteCompanyService;
import com.flycms.modules.system.service.SystemModuleService;
import com.flycms.modules.system.service.ConfigureService;
import com.flycms.modules.template.entity.SiteTemplatePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 前台分类管理
 * @email 79678111@qq.com
 * @Date: 15:26 2019/10/8
 */
@Controller
public class ColumnFrontController extends BaseController {

    @GetMapping("/{moduleType}/index{url.suffix}")
    public String columnShow(@PathVariable(value = "id", required = false) String id,
                             @RequestParam(value = "p", defaultValue = "1") int p,
                             Model model)
    {
        String domainString= StringUtils.twoStageDomain(request.getRequestURL().toString());
        Site site = null;
        if (!org.springframework.util.StringUtils.isEmpty(domainString)) {
            site = siteService.findByDomain(domainString);
        }else{
            site = siteService.findById(Long.parseLong(systemService.findByKeyCode("default_site")));
        }
        if(site!=null){
            if(site.getTemplateId()==null){
                return templateService.getException(model,"设置模板，请进入模板市场进行购买！");
            }
            SiteTemplatePage siteTemplate=siteTemplateService.findById(site.getTemplateIndex());
            if(siteTemplate==null){
                return templateService.getException(model,"栏目首页模板不存在或者未设置");
            }
            model.addAttribute("site",site);

            return siteTemplateService.getSiteTemplate(site.getId(),siteTemplate.getTemplateFilename());
        }else {
            return "forward:404.do";
        }
    }
}
