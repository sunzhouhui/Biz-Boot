package com.flycms.modules.site.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.ApiAssert;
import com.flycms.common.validator.Sort;
import com.flycms.modules.site.entity.SiteColumn;
import com.flycms.modules.site.service.ContentService;
import com.flycms.modules.site.service.SiteColumnService;
import com.flycms.modules.site.service.SiteService;
import com.flycms.modules.site.service.SiteCompanyService;
import com.flycms.modules.system.entity.SystemModule;
import com.flycms.modules.system.service.SystemModuleService;
import com.flycms.modules.system.service.ConfigureService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 前台用户访问页面
 *
 * @author 孙开飞
 */
@Controller
@RequestMapping("/member/content")
public class ContentMemberController extends BaseController {
    @Autowired
    private SiteService siteService;

    @Autowired
    private SiteCompanyService siteCompanyService;

    @Autowired
    private SiteColumnService siteColumnService;
    @Autowired
    protected ConfigureService systemService;

    @Autowired
    private ContentService contentService;

    @Autowired
    private SystemModuleService systemModuleService;

    @GetMapping("/mainlist{url.suffix}")
    public String mainlist(@RequestParam(value = "siteid", required = false) String siteid, Model model)
    {
        ApiAssert.notTrue(siteid == null || !NumberUtils.isNumber(siteid), "网站id为空或者类型错误");
        ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(siteid)), "您不是网站管理员，请勿非法操作");
        //网站id
        model.addAttribute("siteid",siteid);
        return "system/member/site/content/main_list";
    }

    @GetMapping("/list{url.suffix}")
    public String list(@RequestParam(value = "id" , required = false) String id, Model model) throws IOException {
        ApiAssert.notTrue(id == null || !NumberUtils.isNumber(id), "分类id为空或者类型错误");
        SiteColumn column=siteColumnService.findById(Long.parseLong(id));
        ApiAssert.notTrue(column == null, "分类不存在");
        ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(column.getSiteId()), "您不是网站管理员，请勿非法操作");
        SystemModule module  = systemModuleService.findById(column.getChannel());
        if(module != null){
            model.addAttribute("moduleType",module.getModuleType());
        }
        model.addAttribute("siteId",column.getSiteId());
        model.addAttribute("id",id);
        return "system/member/site/content/list";
    }

    @GetMapping("/listData{url.suffix}")
    @ResponseBody
    public Object siteList(@RequestParam(value = "id", defaultValue = "0") String id,
                           @RequestParam(value = "siteId", required = false) String siteId,
                           @RequestParam(value = "title", required = false) String title,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                           @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {
        ApiAssert.notTrue(id == null || !NumberUtils.isNumber(id), "分类id为空或者类型错误");
        if (!StringUtils.isEmpty(siteId)) {
            ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(siteId)), "您不是网站管理员，请勿非法操作");
            ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");
        }
        return contentService.selectContentListPager(siteId,id,title,page,pageSize,sort,order);
    }
}
