package com.flycms.modules.site.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.common.pager.Pager;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.system.entity.Configure;
import com.flycms.modules.user.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface SiteDao extends BaseDao<Site> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    public int deleteSiteByIds(Long[] id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 按id更新网站信息
     *
     * @param site
     * @return
     */
    public int updateSiteById(Site site);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询当前用户网站名称是否重名
     *
     * @param siteName
     *         网站名称
     * @param id
     *         需要排除的网站id
     * @return
     */
    public int checkSiteByName(@Param("siteName") String siteName,@Param("id") Long id);

    /**
     * 查询domain短域名是否被占用
     *
     * @param domain
     *         系统分配短域名
     * @param id
     *         需要排除的网站id
     * @return
     */
    public int checkSiteByDomain(@Param("domain") String domain,@Param("id")Long id);

    /**
     * 按网站Domain查询网站信息
     *
     * @param domain
     * @return
     */
    public Site findByDomain(@Param("domain") String domain);

    /**
     * 按keyCode查询对应的value信息
     *
     * @param keyCode
     * @return
     */
    public Configure findByKeyCode(@Param("keyCode") String keyCode);


    /**
     * 按网站id和用户id查询网站信息
     *
     * @param siteId
     *         网站id
     * @param companyId
     *         用户id
     * @return
     */
    public Site findSiteByCompanyId(@Param("siteId")Long siteId,@Param("companyId")Long companyId);

    /**
     * 按网站id查询关联的用户信息
     *
     * @param siteId
     * @return
     */
    public User selectSiteUserBySiteId(@Param("siteId")Long siteId);

    /**
     * 按用户id查询所有的所有管理的网站
     *
     * @param pager
     * @return
     */
    public int queryUserSiteByTotal(Pager pager);

    /**
     * 按用户id查询所有的所有管理的网站
     *
     * @param pager
     * @return
     */
    public List<Site> queryUserSiteByList(Pager pager);
}
