package com.flycms.modules.company.service;

import com.flycms.modules.company.entity.Company;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:10 2019/8/17
 */
public interface CompanyService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 用户添加企业信息或者编辑企业信息
     * 当系统使用登录用户查询是否已关联企业基本信息，没关联的就将体检的企业基本信息做添加处理
     * 如果关联了就做修改处理
     *
     * @param company
     * @return
     */
    public Object addAndModifyCompany(Company company);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除企业信息
     *
     * @param id
     * @return
     */
    public int deleteById(Long id);

    /**
     * 按企业id删除企业与用户关联信息
     *
     * @param companyId
     * @return
     */
    public int deleteCompanyUserByCompanyId(Long companyId);

    /**
     * 按用户id删除企业与用户关联信息
     *
     * @param userId
     * @return
     */
    public int deleteCompanyUserByUserId(Long userId);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询企业全称是否已存在
     *
     * @param fullName
     *         公司简称
     * @param id
     *         需要排除的id
     * @return
     */
    public boolean checkCompanyByFullName(String fullName,Long id);

    /**
     * 查询企业简称是否已存在
     *
     * @param shortName
     *         公司简称
     * @param id
     *         需要排除的id
     * @return
     */
    public boolean checkCompanyByShortName(String shortName,Long id);

    /**
     * 查询当前用户和企业是否已关联
     *
     * @param companyId
     * @param userId
     * @return
     */
    public boolean checkCompanyByUser(Long companyId,Long userId);

    /**
     * 按用户id查询企业信息
     *
     * @param companyId
     * @return
     */
    public Company findById(Long companyId);

    /**
     * 按用户id查询企业信息
     *
     * @param userId
     * @return
     */
    public Company findCompanyByUser(Long userId);


    /**
     * 查询网站所属帮助信息
     *
     * @param industry
     *         行业id
     * @param fullName
     *         公司全称
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Object queryCompanyPager(Long industry, String fullName, Integer page, Integer pageSize, String sort, String order);
}
