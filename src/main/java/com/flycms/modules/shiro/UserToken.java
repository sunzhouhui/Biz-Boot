package com.flycms.modules.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 0:43 2019/8/28
 */
public class UserToken extends UsernamePasswordToken
{
    public UserToken(String username, String password)
    {
        super(username, password);
    }
}
