package com.flycms.modules.news.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 */
@Setter
@Getter
public class NewsVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long siteId;
	private String siteName;
	private Long columnId;
	private String columnTitle;
	private Integer newsType;
	private String title;
	private String shorttitle;
	private String titlepic;
	private String writer;
	private String source;
	private String content;
	private String description;
	private String keywords;
	private Integer countDigg;
	private Integer countBurys;
	private Integer countView;
	private Integer countComment;
	private Integer status;
	private Long userId;
	private String nickname;	    //昵称
	private LocalDateTime addTime;
	private Integer legitimate;
}