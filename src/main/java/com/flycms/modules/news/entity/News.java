package com.flycms.modules.news.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 */
@Setter
@Getter
public class News implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long siteId;
	private Long columnId;
	private Integer newsType;
	private String title;
	private String shorttitle;
	private String titlepic;
	private String writer;
	private String source;
	private String content;
	private String description;
	private String keywords;
	private Integer countDigg;
	private Integer countBurys;
	private Integer countView;
	private Integer countComment;
	private Integer status;
	private Long userId;
	private LocalDateTime createTime;
	private LocalDateTime updateTime;
	private Integer legitimate;
	private Integer deleted;
}