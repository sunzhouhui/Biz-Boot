package com.flycms.modules.news.controller;

import com.flycms.common.controller.BaseController;
import org.springframework.stereotype.Controller;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 12:40 2019/12/11
 */
@Controller
public class NewsFrontController extends BaseController {

}
