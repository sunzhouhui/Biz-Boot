package com.flycms.modules.user.dao;

import com.flycms.common.pager.Pager;
import com.flycms.modules.user.entity.UserInvite;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInviteDao {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 新增绑定关系
     * @param record
     * @return
     */
    int insertUserInvite(UserInvite record);





    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    int updateByPrimaryKeySelective(UserInvite record);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询当前用户 是否绑定了上级
     * @param id
     * @return
     */
    List<UserInvite> selectInviteByToUserId(Long id);


    UserInvite selectByPrimaryKey(Long id);

    /**
     * 查询当前用户所有下级
     * @param fromUserId
     * @return
     */
    List<Long> selectByFromUserId(Long fromUserId);

    /**
     * 按条件查询用户邀请记录总数
     *
     * @param pager
     * @return
     */
    public Integer selectUserInviteCount(Pager pager);

    /**
     * 按条件查询用户记录列表
     *
     * @param pager
     * @return
     */
    public List<UserInvite> selectUserInviteList(Pager pager);

    /**
     * 查询今日邀请
     * @return
     */
    int getTodayCount(@Param("userId") Long userId);

    /**
     * 查询昨日邀请
     * @return
     */
    int getYesterdayCount(@Param("userId") Long userId);

    /**
     * 查询邀请总人数
     * @param userId
     * @return
     */
    int getTotalCountByFromUser(@Param("userId") Long userId);
}