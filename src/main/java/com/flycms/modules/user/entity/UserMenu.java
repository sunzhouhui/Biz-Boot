package com.flycms.modules.user.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 菜单权限表 fly_menu
 * 
 * @author 孙开飞
 */
@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserMenu extends BaseEntity {

    /** 菜单ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /** 菜单名称 */
    @NotBlank(message = "菜单名称不能为空")
    @Size(min = 0, max = 50, message = "菜单名称长度不能超过50个字符")
    private String menuName;

    /** 父菜单名称 */
    private String parentName;

    /** 父菜单ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /** 显示顺序 */
    @NotBlank(message = "显示顺序不能为空")
    private String sortOrder;

    /** 菜单URL */
    @Size(min = 0, max = 200, message = "请求地址不能超过200个字符")
    private String url;

    /** 打开方式：0页签 1新窗口 */
    private int target;

    /** 类型:M目录, C菜单, F按钮 */
    @NotBlank(message = "菜单类型不能为空")
    private String menuType;

    /** 菜单状态:0显示,1隐藏 */
    private Boolean visible;

    /** 权限字符串 */
    @Size(min = 0, max = 100, message = "权限标识长度不能超过100个字符")
    private String perms;

    /** 菜单图标 */
    private String icon;

    /** 子菜单 */
    private List<UserMenu> children = new ArrayList<UserMenu>();

}
