package com.flycms.modules.user.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 *
 *
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 20:28 2018/8/26
 */
@Getter
@Setter
public class UserActivation implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    //信息类型，0手机，1邮箱
    private int infoTyoe;
    //当前使用验证码的用户Id，可为空
    private Long userId;
    //用户名，手机或者邮箱
    private String userName;
    //验证码
    private String code;
    //注册码类型：1手机注册验证码,2安全手机设置验证码,3密码重置验证码
    private Integer codeType;
    //创建时间
    private LocalDateTime createTime;
    //激活状态，0未激活，1已激活
    private Integer referStatus;
}
