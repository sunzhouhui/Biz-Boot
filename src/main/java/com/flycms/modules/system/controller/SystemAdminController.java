package com.flycms.modules.system.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.site.service.impl.SiteServiceImpl;
import com.flycms.modules.system.entity.Configure;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:50 2019/8/19
 */
@Controller
@RequestMapping("/admin/system")
public class SystemAdminController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(SiteServiceImpl.class);


    @GetMapping("/update_mail{url.suffix}")
    public String updateMail(Model model)
    {
        return"system/admin/system/email";
    }

    /**u
     * 修改用户信息设置配置
     */
    @ResponseBody
    @PostMapping(value = "/update_mail{url.suffix}")
    public Object updateMail(
            @RequestParam(value = "user_reg") String user_reg,
            @RequestParam(value = "user_reg_verify") String user_reg_verify,
            @RequestParam(value = "user_activation_role") String user_activation_role,
            @RequestParam(value = "user_role") String user_role,
            @RequestParam(value = "user_question_verify") String user_question_verify,
            @RequestParam(value = "user_answer_verify") String user_answer_verify
    ) {
        Result data = Result.failure("操作失败");
        try {
            if (!NumberUtils.isNumber(user_role)) {
                return data=Result.failure("权限参数错误");
            }
            if (!NumberUtils.isNumber(user_question_verify)) {
                return data=Result.failure("问答审核参数错误");
            }
            if (!NumberUtils.isNumber(user_answer_verify)) {
                return data=Result.failure("回答审核参数错误");
            }
            systemService.updateKeyValue("user_reg", user_reg);
            systemService.updateKeyValue("user_reg_verify", user_reg_verify);
            systemService.updateKeyValue("user_activation_role", user_activation_role);
            systemService.updateKeyValue("user_role", user_role);
            systemService.updateKeyValue("user_question_verify", user_question_verify);
            systemService.updateKeyValue("user_answer_verify", user_answer_verify);
            return data.success("操作成功", Result.NOOP);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            data.failure("未知错误！");
        }
        return data;
    }

    @GetMapping("/add_system{url.suffix}")
    public String addSystem(Model model) {
        return "system/admin/system/add_system";
    }

    @PostMapping("/add_system{url.suffix}")
    @ResponseBody
    public Object addSystem(Configure system){
        if(StringUtils.isEmpty(system.getKeyCode())){
            return Result.failure("系统配置Key不能为空");
        }
        if(systemService.checkSystemByKeycode(system.getKeyCode(),null)){
            return Result.failure("该系统配置Key已存在");
        }
        return systemService.addSystem(system);
    }

    //批量删除角色
    @PostMapping("/del_system{url.suffix}")
    @ResponseBody
    public Object deleteSystemByIds(String ids){
        if(StringUtils.isEmpty(ids)){
            return Result.failure("系统配置id为空或者不存在");
        }
        return systemService.deleteByIds(ids);
    }

    //更新角色权限页面
    @RequestMapping("/update_system{url.suffix}")
    public String updateSystem(@RequestParam(value = "id", required = false) String id,Model model){
        if (!NumberUtils.isNumber(id)) {
            return "redirect:/404.do";
        }
        Configure system=systemService.findById(Long.parseLong(id));
        model.addAttribute("system",system);
        return "system/admin/system/update_system";
    }

    //修改管理员角色
    @PostMapping("/update_system${url.suffix}")
    @ResponseBody
    public Object updateSystem(Configure system){
        if(org.springframework.util.StringUtils.isEmpty(system.getId())){
            return Result.failure("系统配置id不能为空");
        }
        if(systemService.checkSystemByKeycode(system.getKeyCode(),system.getId())){
            return Result.failure("该系统配置Key已存在");
        }
        return systemService.updateSystem(system);
    }

    //管理员角色列表页面
    @RequiresPermissions("system:list:view")
    @GetMapping("/system_list{url.suffix}")
    public String systemList()
    {
        return"system/admin/system/list_system";
    }

    //管理员角色列表json翻页数据
    @RequiresPermissions("system:list:view")
    @GetMapping("/system_listData{url.suffix}")
    @ResponseBody
    public Object systemlistData(Configure system,
                               @RequestParam(value = "page",defaultValue = "1") Integer page,
                               @RequestParam(value = "limit",defaultValue = "10") Integer limit,
                               @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "id") String sort,
                               @RequestParam(defaultValue = "desc") String order)
    {
        return systemService.selectSystemListLayPager(system, page, limit, sort, order);
    }
}
