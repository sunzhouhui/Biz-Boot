package com.flycms.modules.notify.dao;
import com.flycms.common.dao.BaseDao;
import com.flycms.modules.notify.entity.EmailTemplet;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2018/7/7
 */
@Repository
public interface EmailTempletDao extends BaseDao<EmailTemplet> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    //修改邮件模板
    public int updateEmailTempletsById(EmailTemplet email);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    //按模板标记码查询配置信息
    public EmailTemplet findEmailTempletByTpCode(@Param("tpCode") String tpCode);

}
