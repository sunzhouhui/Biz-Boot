package com.flycms.modules.notify.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class EmailTemplet implements Serializable {
    private static final long serialVersionUID = 1L;
    //用户id
    private Integer id;
    //模板标记码
    private String tpCode;
    //邮件标题
    private String title;
    //邮件内容
    private String content;
}