package com.flycms.modules.product.controller;

import com.flycms.common.controller.BaseController;
import org.springframework.stereotype.Controller;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 前台网站产品访问页面
 * @email 79678111@qq.com
 * @Date: 12:40 2019/12/11
 */
@Controller
public class ProductFrontController extends BaseController {
    
}
