package com.flycms.modules.product.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 */
@Setter
@Getter
public class ProductDO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String siteId;
	private String columnId;
	private Integer newsType;
	private String title;
	private String shorttitle;
	private String titlepic;
	private String writer;
	private String source;
	private String content;
	private String description;
	private String keywords;
	private Integer status;
}